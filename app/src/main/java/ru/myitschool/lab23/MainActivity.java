package ru.myitschool.lab23;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    String[] korean = {"안녕하세요", "감사합니다", "사과", "책", "개", "고양이"};
    String[] russian = {"привет", "спасибо", "яблоко", "книга", "собака", "кошка"};

    TextView textView;

    int wordNow = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.word);
        textView.setText(korean[wordNow]);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(russian[wordNow]);
                Thread thread = new Thread(){
                    @Override
                    public void run(){
                        try {
                            Thread.sleep(5000);
                            wordNow++;
                            if (wordNow < korean.length){
                                textView.setText(korean[wordNow]);
                            }
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                };
                thread.start();
            }
        });
    }
}
